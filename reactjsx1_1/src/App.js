import './App.css';
import React from 'react';
import Heaader from './Header';
import Content from './Content';
import Footer from './Footer';

class App extends React.Component {
  render() {
    return <div>
    <Heaader />
    <Content />
    <Footer />
  </div>
  }
}

export default App;
