import React from "react";
import './Style.css';


const colors = ['red', 'blue', 'green', 'purple', 'pink'];
const min = 0;
const max = colors.length;

const RandomNumberColor = () => Math.floor(Math.random() *(max - min) + min)
const color = colors[RandomNumberColor()];

const randomFontSize = () => Math.floor(Math.random()*20+20);
const fontSize = randomFontSize();


class RandomBox extends React.Component {
    render() {
        return (
            <div className="center" style={{backgroundColor: color}}>
                <p style={{fontSize : fontSize}}>Rendom Box</p>
            </div>
        );
    }
}
export default RandomBox