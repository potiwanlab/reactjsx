import logo from './logo.svg';
import './App.css';
import RandomBox from './Component/RandomBox';

function App() {
  return (
    <div>
      <RandomBox />
    </div>
  );
}

export default App;
