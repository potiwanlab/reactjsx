import logo from './logo.svg';
import './App.css';
import Navbar from './Component/Navbar'
import Footer from './Component/Footer';
import PictureCard from './Component/PictureCard';
const row = [0, 1, 2, 3, 4, 5, 6]
function App() {
  return (
    <div>
    <Navbar />
    <div className='row'>
      {row.map(c=>(
        <div className='col-md-3'><PictureCard /></div>
      ))}
    </div>
        
    <Footer/>
  </div>
  );
}

export default App;
