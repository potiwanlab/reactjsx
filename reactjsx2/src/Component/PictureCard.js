import React from "react";
import './Style.css';
import logo from './pikkanode.png'

class PictureCard extends React.Component {
    render() {
        return (
            <div className="card">
                <img src={logo} alt='Logo' width={140} height={70} />
                <div class="container">
                    <h4>วันที่ 20/12/2021</h4>
                    <p>like:20</p>
                    <p>comment:50</p>
                </div>
            </div>

        );
    }
}
export default PictureCard