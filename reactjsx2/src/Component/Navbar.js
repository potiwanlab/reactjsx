import React from "react";
import './Style.css';
import logo from './pikkanode.png'

class Navbar extends React.Component {
    render() {
        return (
            <div>
                <ul>
                    <li><img className="image" src = {logo} alt = 'Logo' width={70} height={35}></img></li>
                    <li><a href="#create">Create pikka</a></li>
                    <li style={{float : "right"}}><a class="active" href="#signin">sign in</a></li>
                    <li style={{float : "right"}}><a href="#signup">sign up</a></li>
                    <li style={{float : "right"}}><a href="#signout">sign out</a></li>      
                </ul>
            </div>
        );
    }
}
export default Navbar
